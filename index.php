<?php
        include("includes/identifiant.php");

        $error = false;
        if(isset($_POST['submit']) && !empty($_POST['submit'])){
            // That could be way more complex. Mail/Phone validation etc ...
            if (!empty($_POST['name']) && !empty($_POST['mail']) && !empty($_POST['phone']) && !empty($_POST['message'])) {


                    //getting our form's values.
                    $name = $_POST['name'];
                    $mail = $_POST['mail'];
                    $phone = $_POST['phone'];
                    $message = $_POST['message'];
                    $ip = $_SERVER['REMOTE_ADDR']; //not perfect, cause it may display proxy's IP.
                    //Building and sending an e-mail
                    $body = "Mr/Mme ".$name." a envoyé le message suivant :\n".$message."\n\nAdresse mail : ".$mail."\nN° de téléphone : ".$phone;
                    $body = wordwrap($body, 70);
                    mail('acme@example.com', 'Nouveau message du formulaire de test',$body, "From: {$_POST['mail']}");

                    //Inserting a save into a Database.
                    $query=$db->prepare('INSERT INTO user_message 
                    (`user_name`, `user_mail`, `user_phone`, `message`, `user_ip`)
                    VALUES(:username, :mail, :phone, :mess, :ip)');
                    $query->bindValue(':username', $name, PDO::PARAM_STR);
                    $query->bindValue(':mail', $mail, PDO::PARAM_STR);
                    $query->bindValue(':phone', $phone, PDO::PARAM_STR);
                    $query->bindValue(':mess', $message, PDO::PARAM_STR);
                    $query->bindValue(':ip', $ip, PDO::PARAM_STR);
                    $query->execute();

                    //relocating the user, using URL to confirm that the message is gone.
                    header('Location: index.php?m=sent'); 
            }  else {
                //Using a variable so that if some fields are empty we can display an error message.
                $error = true;
            }
        }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="./css/style.css">
    <title>Document</title>
</head>
<body>
        <h1>Formulaire Php du fennec 2.0 !</h1>

        <form action="index.php" method="POST">
            <label for="name">Nom/prénom :</label>
            <input type="text" name="name" id="name">
            <div class="sub-field">
            	<div>
                    <label for="mail">Votre Mail :</label>
                    <input type="text" name="mail" id="mail">
                </div>
            	<div>
                    <label for="phone">Votre N° de téléphone : </label>
                    <input type="text" name="phone" id="phone">
                </div>
            </div>
            <label for="message">Votre Message :</label>
            <textarea name="message" id="message" cols="30" rows="10"></textarea>
            <input type="submit" name="submit" value="Envoyer">
        <?php if($error){
                echo '<div class="error">Veuillez remplir tout les champs</div>';
            } 
            if(isset($_GET['m']) && $_GET['m']=='sent'){
                echo '<div class="info">Message Envoyé !</div>';
            }?>
        </form>
    <script src="./js/main.js"></script>
</body>
</html>
